// Power switch implementation which uses the tsmem gRPC Server
package power

import (
	pb "bitbucket.org/mfkenney/tsmem"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"time"
)

type TsmemPowerSwitch struct {
	conn    *grpc.ClientConn
	client  pb.TsmemClient
	ctx     context.Context
	timeout time.Duration
	name    string
}

func NewTsmemPowerSwitch(name string) (*TsmemPowerSwitch, error) {
	var err error
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	p := new(TsmemPowerSwitch)
	p.ctx = context.Background()
	p.timeout = time.Second * 2
	p.name = name
	p.conn, err = grpc.Dial("127.0.0.1:10000", opts...)
	if err != nil {
		return nil, err
	}
	p.client = pb.NewTsmemClient(p.conn)

	return p, nil
}

func (p *TsmemPowerSwitch) On() error {
	ctx, cancel := context.WithTimeout(p.ctx, p.timeout)
	defer cancel()
	msg := pb.DioMsg{
		Name:  p.name,
		State: pb.DioState_HIGH,
	}
	_, err := p.client.DioSet(ctx, &msg)
	return err
}

func (p *TsmemPowerSwitch) Off() error {
	ctx, cancel := context.WithTimeout(p.ctx, p.timeout)
	defer cancel()
	msg := pb.DioMsg{
		Name:  p.name,
		State: pb.DioState_LOW,
	}
	_, err := p.client.DioSet(ctx, &msg)
	return err
}

func (p *TsmemPowerSwitch) Test() (bool, error) {
	ctx, cancel := context.WithTimeout(p.ctx, p.timeout)
	defer cancel()
	msg := pb.DioMsg{Name: p.name}
	resp, err := p.client.DioGet(ctx, &msg)
	if err != nil {
		return false, err
	}
	return resp.GetState() == pb.DioState_HIGH, nil
}
