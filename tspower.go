//
package power

import (
	"bytes"
	"os/exec"
	"strings"
)

// TsPowerSwitch implements a power switch using the
// 'tsctl' program on a Technologic Systems CPU board.
type TsPowerSwitch struct {
	name       string
	tsctl_path string
	class      string
	getcmd     string
	setcmd     string
}

func NewTsPowerSwitch(name string) *TsPowerSwitch {
	path, err := exec.LookPath("tsctl")
	if err != nil {
		return nil
	}

	sw := TsPowerSwitch{
		name:       name,
		tsctl_path: path,
		class:      "DIO",
		getcmd:     "getasync",
		setcmd:     "setasync",
	}

	return &sw
}

func (sw *TsPowerSwitch) set(val int) error {
	var state string
	if val > 0 {
		state = "high"
	} else {
		state = "low"
	}
	cmd := exec.Command("sudo", sw.tsctl_path,
		sw.class, sw.setcmd, sw.name, state)
	cmd.Stdout = nil
	cmd.Stderr = nil
	err := cmd.Run()
	return err
}

func (sw *TsPowerSwitch) get() (string, error) {
	cmd := exec.Command("sudo", sw.tsctl_path,
		sw.class, sw.getcmd, sw.name)
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = nil
	err := cmd.Run()
	return strings.TrimRight(out.String(), " \t\r\n"), err
}

// Set switch to the ON state
func (sw *TsPowerSwitch) On() error {
	return sw.set(1)
}

// Set the switch to the OFF state
func (sw *TsPowerSwitch) Off() error {
	return sw.set(0)
}

// Return the state of the switch
func (sw *TsPowerSwitch) Test() (bool, error) {
	out, err := sw.get()
	if err == nil {
		parts := strings.Split(out, "=")
		return parts[1] == "1" || parts[1] == "HIGH", nil
	}
	return false, err
}
