//

package power

import (
	"bitbucket.org/mfkenney/go-tsctl"
	"net"
	"strconv"
)

// TsctlPowerSwitch implements a power switch using the
// 'tsctl' TCP interface.
type TsctlPowerSwitch struct {
	name   string
	dionum uint32
	conn   net.Conn
}

func NewTsctlPowerSwitch(name string) *TsctlPowerSwitch {
	var value uint64
	var err error

	conn, err := net.Dial("tcp", "localhost:5001")
	if err != nil {
		return nil
	}

	value, err = strconv.ParseUint(name, 10, 32)
	if err != nil {
		var reply tsctl.ScalarReply
		buf, err := tsctl.MapLookupMsg(name)
		if err != nil {
			return nil
		}

		_, err = conn.Write(buf)
		if err != nil {
			return nil
		}

		err = tsctl.UnpackReply(conn, &reply)
		if reply.Value < 0 {
			return nil
		}
		value = uint64(reply.Value)
	}

	return &TsctlPowerSwitch{name: name, dionum: uint32(value), conn: conn}
}

func (sw *TsctlPowerSwitch) On() error {
	var reply tsctl.TsReply
	buf, err := tsctl.DioSetAsyncMsg(sw.dionum, tsctl.High)
	_, err = sw.conn.Write(buf)
	if err == nil {
		err = tsctl.UnpackReply(sw.conn, &reply)
	}
	return err
}

func (sw *TsctlPowerSwitch) Off() error {
	var reply tsctl.TsReply
	buf, err := tsctl.DioSetAsyncMsg(sw.dionum, tsctl.Low)
	_, err = sw.conn.Write(buf)
	if err == nil {
		err = tsctl.UnpackReply(sw.conn, &reply)
	}
	return err
}

func (sw *TsctlPowerSwitch) Test() (bool, error) {
	var reply tsctl.ScalarReply
	var test bool = false

	buf, err := tsctl.DioGetAsyncMsg(sw.dionum)
	_, err = sw.conn.Write(buf)
	if err == nil {
		err = tsctl.UnpackReply(sw.conn, &reply)
		test = tsctl.DioState(reply.Value) == tsctl.High
	}

	return test, err
}
