// Package power provides a power control interface
// for peripheral sensors
package power

type PowerSwitch interface {
	On() error
	Off() error
	Test() (bool, error)
}

type MockSwitch struct {
	name  string
	state bool
}

func NewMockSwitch(name string) *MockSwitch {
	return &MockSwitch{name: name, state: false}
}

func (sw *MockSwitch) On() error {
	sw.state = true
	return nil
}

func (sw *MockSwitch) Off() error {
	sw.state = false
	return nil
}

func (sw *MockSwitch) Test() (bool, error) {
	return sw.state, nil
}
